RSpec.describe 'Products', type: :request do
  describe "GET #show" do
    include_context 'product setup'
    let!(:related_products) do
      create_list(
        :product,
        Potepan::ProductsController::MAX_COUNT_TO_DISPLAY + 1,
        name: "Ruby Shoes",
        taxons: [taxon]
      )
    end

    before(:each) { get potepan_product_path product.id }

    it { expect(response).to have_http_status(:success) }

    context '商品詳細があるとき' do
      it { expect(response.body).to include "#{product.name}" }
      it { expect(response.body).to include "#{product.display_price}" }
      it { expect(response.body).to include "#{product.description}" }
    end

    context '関連商品があるとき' do
      it { expect(response.body).to include "#{related_products.first.name}" }
      it { expect(response.body).to include "#{related_products.first.display_price}" }
    end

    context '関連商品がないとき' do
      let!(:related_products) { [] }

      it { expect(response.body).to include "関連商品はありません。" }
    end
  end
end

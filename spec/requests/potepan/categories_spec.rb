RSpec.describe 'Categories', type: :request do
  describe "GET #show" do
    include_context 'product setup'

    before(:each) { get potepan_category_path taxon.id }

    it { expect(response).to have_http_status(:success) }

    it { expect(response.body).to include taxonomy.name }

    it { expect(response.body).to include taxon.name }
  end
end

RSpec.describe 'ProductsShow', type: :system do
  include_context 'product setup'
  let!(:related_products) do
    create_list(
      :product,
      Potepan::ProductsController::MAX_COUNT_TO_DISPLAY + 1,
      name: "Ruby Shoes",
      taxons: [taxon]
    )
  end

  before(:each) { visit potepan_product_path product.id }

  describe 'ヘッダーのテスト' do
    it 'サイトロゴをクリックするとトップページへ飛ぶ' do
      click_link 'site-logo'
      expect(current_path).to eq potepan_path
    end

    it 'HOMEをクリックするとトップページへ飛ぶ' do
      within '.header' do
        click_link 'home'
        expect(current_path).to eq potepan_path
      end
    end

    it 'カートロゴをクリックするとカートページへ飛ぶ' do
      find('#cart-link').click
      expect(current_path).to eq potepan_cart_page_path
    end

    it '虫眼鏡アイコンにカーソルを乗せると、検索フォームを表示する', js: true do
      find('.searchBox').hover
      expect(page).to have_selector 'span', text: "検索"
    end
  end

  describe 'lightSection(pageHeader)のテスト' do
    it 'HOMEをクリックするとトップページへ飛ぶ' do
      within '.pageHeader' do
        click_link 'home'
        expect(current_path).to eq potepan_path
      end
    end

    it 'タイトルとパンくずリストに、商品ごとの名前が表示されている' do
      within '.pageHeader' do
        expect(page).to have_selector 'h2', text: product.name
        expect(page).to have_selector '.active', text: product.name
      end
    end
  end

  describe 'メインコンテンツのテスト' do
    it '「一覧ページへ戻る」をクリックすると、商品が属するtaxonの最初の一覧ページへ飛ぶ' do
      click_link '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path product.taxons.first.id
    end

    it '商品名、価格、説明文が表示される' do
      within '.media-body' do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
        expect(page).to have_content product.description
      end
    end
  end

  describe '関連商品のテスト' do
    it "関連商品が#{Potepan::ProductsController::MAX_COUNT_TO_DISPLAY + 1}個あっても、
    #{Potepan::ProductsController::MAX_COUNT_TO_DISPLAY}個までしか表示しない" do
      within '.productsContent' do
        expect(page).
          to have_selector '.productBox', count: Potepan::ProductsController::MAX_COUNT_TO_DISPLAY
      end
    end
  end

  describe 'フッターのテスト' do
    it { expect(page).to have_css '#footer' }
  end
end

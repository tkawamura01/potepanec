RSpec.describe 'CategoriesShow', type: :system do
  include_context 'product setup'

  before(:each) { visit potepan_category_path taxon.id }

  describe 'ヘッダーのテスト' do
    it 'サイトロゴをクリックするとトップページへ飛ぶ' do
      click_link 'site-logo'
      expect(current_path).to eq potepan_path
    end

    it 'HOMEをクリックするとトップページへ飛ぶ' do
      within '.header' do
        click_link 'home'
        expect(current_path).to eq potepan_path
      end
    end

    it 'カートロゴをクリックするとカートページへ飛ぶ' do
      find('#cart-link').click
      expect(current_path).to eq potepan_cart_page_path
    end

    it '虫眼鏡アイコンにカーソルを乗せると、検索フォームを表示する', js: true do
      find('.searchBox').hover
      expect(page).to have_selector 'span', text: "検索"
    end
  end

  describe 'lightSection(pageHeader)のテスト' do
    it 'HOMEをクリックするとトップページへ飛ぶ' do
      within '.pageHeader' do
        click_link 'home'
        expect(current_path).to eq potepan_path
      end
    end

    it 'shopリンクが表示されている' do
      within '.pageHeader' do
        expect(page).to have_link 'shop'
      end
    end

    it 'タイトルとパンくずリストに、商品カテゴリーごとの名前が表示されている' do
      within '.pageHeader' do
        expect(page).to have_selector 'h2', text: taxon.name
        expect(page).to have_selector '.active', text: taxon.name
      end
    end
  end

  describe '並び替えのドロップダウンメニューのテスト' do
    it { expect(page).to have_selector 'select', text: "並び替え" }
  end

  describe 'GRIDとLISTの切替ボタンのテスト' do
    it do
      expect(page).to have_selector 'button', text: "Grid"
      expect(page).to have_selector 'button', text: "List"
    end
  end

  describe 'レフトサイドバーのテスト' do
    it '個別カテゴリーをクリック、taxonがドロップダウン表示、taxon名をクリック、一覧ページへ飛ぶ', js: true do
      within '.sideBar' do
        click_on taxonomy.name
        expect(page).to have_link taxon.name
        click_on taxon.name
        expect(current_path).to eq potepan_category_path taxon.id
      end
    end
  end

  describe '商品の表示テスト' do
    it do
      within '.productBox' do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
      end
    end
  end

  describe 'lightSection(pageFooter)のテスト' do
    it { expect(page).to have_css '.pageFooter' }
  end

  describe 'フッターのテスト' do
    it { expect(page).to have_css '#footer' }
  end
end

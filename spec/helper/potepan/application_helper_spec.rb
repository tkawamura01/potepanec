RSpec.describe ApplicationHelper, type: :helper do
  let(:product) { create(:product) }

  context 'ページタイトルが空白のとき' do
    it { expect(full_title("")).to eq("#{ApplicationHelper::BASE_TITLE}") }
  end

  context 'ページタイトルがnilのとき' do
    it { expect(full_title(nil)).to eq("#{ApplicationHelper::BASE_TITLE}") }
  end

  context 'ページタイトルがあるとき' do
    it {
      expect(full_title("#{product.name}")).
        to eq("#{product.name} - #{ApplicationHelper::BASE_TITLE}")
    }
  end
end

RSpec.shared_context 'product setup' do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, name: "Ruby Bag", taxons: [taxon]) }
end

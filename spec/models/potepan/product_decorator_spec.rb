RSpec.describe Potepan::ProductDecorator, type: :model do
  let(:item) { create(:taxonomy) }
  let(:brand) { create(:taxonomy) }
  let(:bag) { create(:taxon, taxonomy: item) }
  let(:shoes) { create(:taxon, taxonomy: item) }
  let(:belt) { create(:taxon, taxonomy: item) }
  let(:rails) { create(:taxon, taxonomy: brand) }
  let(:laravel) { create(:taxon, taxonomy: brand) }
  let(:django) { create(:taxon, taxonomy: brand) }
  let!(:rails_backpack) { create(:product, taxons: [bag, rails]) }
  let!(:rails_sneakers) { create(:product, taxons: [shoes, rails]) }
  let!(:rails_tote) { create(:product, taxons: [bag, rails]) }
  let!(:laravel_pochette) { create(:product, taxons: [bag, laravel]) }
  let!(:laravel_sandals) { create(:product, taxons: [shoes, laravel]) }
  let!(:django_belt) { create(:product, taxons: [belt, django]) }

  context 'ある商品が2つのtaxonを持つとき' do
    subject { rails_backpack.related_products(Potepan::ProductsController::MAX_COUNT_TO_DISPLAY) }

    it { is_expected.not_to include rails_backpack }

    it '同じ2つのtaxonを持つ商品は関連商品に重複して含まれず、関連商品はidの昇順で取得される' do
      is_expected.to eq [rails_sneakers, rails_tote, laravel_pochette]
    end
  end

  context '関連商品が存在しないとき' do
    subject { django_belt.related_products(Potepan::ProductsController::MAX_COUNT_TO_DISPLAY) }

    it { is_expected.to eq [] }
  end
end

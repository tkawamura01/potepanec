class Potepan::CategoriesController < ApplicationController
  def show
    @taxonomeis = Spree::Taxon.roots
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.all_products.includes(master: [:default_price, :images])
  end
end

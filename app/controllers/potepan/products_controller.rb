class Potepan::ProductsController < ApplicationController
  MAX_COUNT_TO_DISPLAY = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products(MAX_COUNT_TO_DISPLAY)
  end
end

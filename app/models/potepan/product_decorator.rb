module Potepan::ProductDecorator
  def related_products(max_count_to_display)
    Spree::Product.
      in_taxons(taxons).
      distinct.
      where.not(id: id).
      order(:id).
      limit(max_count_to_display)
  end

  Spree::Product.prepend self
end
